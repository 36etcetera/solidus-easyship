class AddCourierIdToShippingMethods < ActiveRecord::Migration[4.2]
  def change
    add_column :spree_shipping_methods, :courier_id, :string
  end
end
