class AddIsAvailableToSpreeShippingRate < ActiveRecord::Migration[5.2]
  def change
    add_column :spree_shipping_rates, :is_available, :boolean, default: true
  end
end
