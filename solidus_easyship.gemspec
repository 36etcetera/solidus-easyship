# encoding: UTF-8
$:.push File.expand_path('../lib', __FILE__)
require 'solidus_easyship/version'

Gem::Specification.new do |s|
  s.name        = 'solidus_easyship'
  s.version     = SolidusEasyship::VERSION
  s.summary     = 'Add extension summary here'
  s.description = 'Add (optional) extension description here'
  s.license     = 'BSD-3-Clause'

  s.author    = 'Mayank Kumar'
  s.email     = 'kumarmayankror@gmail.com'
  s.homepage  = 'https://github.c'

  s.files = Dir["{app,config,db,lib}/**/*", 'LICENSE', 'Rakefile', 'README.md']
  s.test_files = Dir['test/**/*']

  s.add_dependency 'solidus_core'
  s.add_dependency 'solidus_backend'
  s.add_dependency 'rest-client'

  s.add_development_dependency 'capybara'
  s.add_development_dependency 'poltergeist'
  s.add_development_dependency 'coffee-rails'
  s.add_development_dependency 'sass-rails'
  s.add_development_dependency 'database_cleaner'
  s.add_development_dependency 'factory_girl'
  s.add_development_dependency 'rspec-rails'
  s.add_development_dependency 'rubocop', '0.37.2'
  s.add_development_dependency 'rubocop-rspec', '1.4.0'
  s.add_development_dependency 'simplecov'
  s.add_development_dependency 'sqlite3'
end
