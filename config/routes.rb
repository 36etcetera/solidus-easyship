Spree::Core::Engine.routes.draw do
  # Add your extension routes here
  post 'shipment/document_completed', to: 'easyship_shipments#document_completed'

  namespace :admin do
    resources :orders do
      member do
        post :buy_label
        get :shipment_status
      end
    end
  end
end
