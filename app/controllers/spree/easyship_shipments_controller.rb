module Spree
  class EasyshipShipmentsController < ApplicationController
    skip_before_action :verify_authenticity_token, only: :document_completed

    def document_completed
      shipment = Spree::Shipment.find_by(easyship_shipment_id: params[:label][:easyship_shipment_id]) if params[:label]
      # render status: 404, json: { message: "Shipment not found" }.to_json and return unless shipment

      shipment.update_attributes(shipment_params) if shipment
      render status: 200, json: { label_received: true }.to_json
    end

    private

    def shipment_params
      label_params = params[:label]
      {
        tracking_number: label_params[:tracking_number],
        tracking: label_params[:tracking_number],
        tracking_url: label_params[:tracking_page_url],
        shipping_label_url: label_params[:label_url],
        shipping_label_state: 'generated'
      }
    end
  end
end
