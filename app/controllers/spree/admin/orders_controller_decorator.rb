Spree::Admin::OrdersController.class_eval do
  before_action :local_load_order, only: [:buy_label, :shipment_status]
  before_action :check_for_order_completed, only: [:buy_label, :shipment_status]

  def buy_label
    label_response = EasyshipAPI.new(@order, params).buy_label
    reload_page error: label_response['errors'].first and return if label_response['errors']

    label_response['labels'] && label_response['labels'].each do |label|
      shipment = @order.shipments.find_by(easyship_shipment_id: label['easyship_shipment_id'])
      shipment.update_attributes(shipment_label_params label) if shipment
    end

    reload_page notice: label_response['message']
  end

  def shipment_status
    status_response = EasyshipAPI.new(@order, params).status params[:easyship_shipment_id]
    reload_page error: status_response['errors'].first and return if status_response['errors']
    
    status_response['shipments'] && status_response['shipments'].each do |shipment_status|
      Rails.logger.debug "============ Shipment Status update =============="
      Rails.logger.debug "============ #{status_response['shipments']} =============="
      shipment = @order.shipments.find_by(easyship_shipment_id: shipment_status['easyship_shipment_id'])
      if shipment
        shipment.update_attributes(shipment_status_params shipment_status) 
        Rails.logger.debug "============ #{shipment} =============="
        Rails.logger.debug "============ Finished update Shipment Status =============="
      end
    end

    reload_page notice: "Shipment status is updated."
  end

  private

  def local_load_order
    load_order
  end

  def check_for_order_completed
    reload_page error: 'Order is not completed yet.' and return unless @order.completed?
  end

  def reload_page message
    redirect_to(edit_admin_order_path(@order), flash: message)
  end

  def shipment_label_params label
    {
      shipping_label_state: label['label_state'],
      shipping_label_url: label['label_url'],
      tracking_number: label['tracking_number'],
      tracking_url: label['tracking_page_url']
    }
  end

  def shipment_status_params shipment
    {
      shipping_label_state: shipment['status'],
      shipping_label_url: shipment['label_url'],
      tracking_number: shipment['tracking_number'],
      tracking_url: shipment['tracking_page_url']
    }
  end

end
