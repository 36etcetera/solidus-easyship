Spree::CheckoutController.class_eval do
  alias_method :spree_before_delivery, :before_delivery
  after_action :create_easy_shipment, only: :update, if: -> { @order.completed? }
  after_action :update_shipment, only: :update, if: -> { @order.payment? }
  private

  def before_delivery
    spree_before_delivery
    shipment = @order.shipments.first
    refresh_rates(shipment) if params[:refresh_rates]
    # update_shipping if params[:update_shipping] <-- it's now part of refresh_rates
  end

  def update_shipment
    shipment = @order.shipments.first
    shipment_method = shipment.shipping_rates.find_by(selected: true).shipping_method
    shipment.update_attributes({courier_id: shipment_method.courier_id, courier_name: shipment_method.name})    
  end
  
  def create_easy_shipment
    Rails.logger.debug "================================================"
    Rails.logger.debug "========= Create Shipment ========"

        
    shipment = @order.shipments.first
    # @order.assign_attributes order_params

    selected_courier_id = shipment.shipping_rates.find_by(selected: true).shipping_method.courier_id
    redirect_to(checkout_state_path(@order.state)) and return unless selected_courier_id

    user_input = { taxes_duties_paid_by: shipment.taxes_duties_paid_by,
                   is_insured: shipment.is_insured,
                   selected_courier_id: selected_courier_id }
    
    # This is temporary code.
    eligible_shipment_promotion = @order.shipment_adjustments.eligible.size
    Rails.logger.debug "========= Shipment Adjustment #{eligible_shipment_promotion} ========"      

    unless eligible_shipment_promotion > 0
      if shipment && shipment.easyship_shipment_id
        response = EasyshipAPI.new(@order, user_input).update_shipment(shipment.easyship_shipment_id)
      else
        response = EasyshipAPI.new(@order, user_input).create_shipment
      end

      flash[:error] = response['message'] if response['message']
      redirect_to(checkout_state_path(@order.state)) and return unless response['shipment']

      if shipment
        shipment.update_attributes_and_order(shipment_params response['shipment'])
      else
        shipment = @order.shipments.create(shipment_params response['shipment'])
      end
    end
    Rails.logger.debug "========= End Create Shipment ========"    
    Rails.logger.debug "================================================"
  end

  def shipment_params shipment
    {
      easyship_shipment_id: shipment['easyship_shipment_id'],
      # number: shipment['easyship_shipment_id'], <-- Keep the original shipment number from Solidus
      currency: shipment['currency']
    }
    .merge(selected_courier_params shipment['selected_courier'])
  end

  def selected_courier_params selected_courier
    {
      courier_id: selected_courier['id'],
      cost: selected_courier['total_charge'],
      courier_name: selected_courier['name']
    }
    .merge(selected_courier.select { |attr| required_selected_courier_attr.include? attr })
  end

  def required_selected_courier_attr
    [ 'min_delivery_time', 'max_delivery_time', 'shipment_charge',
      'shipment_charge_total', 'insurance_fee', 'import_tax_charge',
      'import_duty_charge', 'ddp_handling_fee' ]
  end

  def refresh_rates shipment
    shipment.update_attributes(tax_payer_insured_params)
    easyship_rates = EasyshipAPI.new(@order, tax_payer_insured_params).get_shipping_rates

    return unless (easyship_rates[:shippers] && easyship_rates[:shippers].any?)

    shipment.shipping_rates.each do |rate|
      new_rate = easyship_rates[:shippers].find { |r| rate.name.eql? r[:courier_name] }

      if new_rate
        rate.update_attributes(cost: new_rate[:total_charge], is_available: true)
      else
        rate.update_attributes(is_available: false)
      end
      
    end
    
    if params[:update_shipping]
      selected_courier_id = params[:selected_courier_id]
  
      # Needs to update the selected courier id
      shipment.update_attributes(selected_shipping_rate_id: selected_courier_id)
  
      selected_courier_shipping_rate = shipment.shipping_rates.find(selected_courier_id)
      unless selected_courier_shipping_rate.is_available?
        easyship_shipping_rate = easyship_rates[:shippers].first
        selected_courier_shipping_rate = shipment.shipping_rates.where(is_available: true).first
        selected_courier_id = selected_courier_shipping_rate.id
      end

      easyship_shipping_rate = easyship_rates[:shippers].find { |r| r[:courier_id].eql? selected_courier_shipping_rate.shipping_method.courier_id }
      shipment_attributes = { cost: easyship_shipping_rate[:total_carge], 
                              courier_id: easyship_shipping_rate[:courier_id],
                              courier_name: easyship_shipping_rate[:courier_name],
                              min_delivery_time: easyship_shipping_rate[:min_delivery_time], 
                              max_delivery_time: easyship_shipping_rate[:max_delivery_time],
                              value_for_money_rank: easyship_shipping_rate[:value_for_money_rank],
                              delivery_time_rank: easyship_shipping_rate[:delivery_time_rank],
                              shipment_charge: easyship_shipping_rate[:shipment_charge],
                              shipment_charge_total: easyship_shipping_rate[:shipment_charge_total],
                              insurance_fee: easyship_shipping_rate[:insurance_fee],
                              import_tax_charge: easyship_shipping_rate[:import_tax_charge],
                              import_duty_charge: easyship_shipping_rate[:import_duty_charge],
                              ddp_handling_fee: easyship_shipping_rate[:ddp_handling_fee],
                              total_charge: easyship_shipping_rate[:total_charge],
                              taxes_duties_paid_by: tax_payer_insured_params[:taxes_duties_paid_by],
                              is_insured: tax_payer_insured_params[:is_insured],
                              selected_shipping_rate_id: selected_courier_id
                            }
      shipment.update_attributes_and_order(shipment_attributes)
      @order.recalculate
    end
  end

  # deprecated method
  def update_shipping shipment
    selected_courier_id = params[:selected_courier_id]
  
    # Needs to update the selected courier id
    shipment.update_attributes(selected_shipping_rate_id: selected_courier_id)
  
    easyship_courier_id = shipment.shipping_rates.find(selected_courier_id).shipping_method.courier_id

    user_input = { taxes_duties_paid_by: tax_payer_insured_params[:taxes_duties_paid_by],
                   is_insured: tax_payer_insured_params[:is_insured],
                   selected_courier_id: easyship_courier_id }
    if shipment && shipment.easyship_shipment_id
      response = EasyshipAPI.new(@order, user_input).update_shipment(shipment.easyship_shipment_id)
    else
      response = EasyshipAPI.new(@order, user_input).create_shipment
    end

    if shipment
      shipment.update_attributes_and_order(shipment_params response['shipment'])
      @order.recalculate
    else
      shipment = @order.shipments.create(shipment_params response['shipment'])
    end
  end

  def tax_payer_insured_params
    {
      taxes_duties_paid_by: params[:taxes_duties_paid_by] || "Sender",
      is_insured: params[:is_insured].present?
    }
  end

  def order_params
    params.require(:order).permit(shipments_attributes: [:selected_shipping_rate_id, :id])
  end
end
