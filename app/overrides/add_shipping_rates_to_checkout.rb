Deface::Override.new(
  virtual_path: 'spree/checkout/_delivery',
  name: 'shipping_rates',
  original: 'f569adff7ba354b62b346110810c38963fe2d959',
  replace: '.shipping-methods',
  partial: 'spree/checkout/easyship_delivery'
)
