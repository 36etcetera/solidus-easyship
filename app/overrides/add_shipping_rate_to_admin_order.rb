Deface::Override.new(
  virtual_path: 'spree/admin/orders/_shipment',
  name: 'shipping_rates_to_admin_order',
  replace: 'tr.edit-shipping-method',
  partial: 'spree/admin/order/easyship_shipment'
)

Deface::Override.new(
  virtual_path: 'spree/admin/orders/_shipment',
  name: 'tracking_to_admin_order',
  replace: 'tr.edit-tracking',
  partial: 'spree/admin/order/easyship_shipment_tracking'
)
