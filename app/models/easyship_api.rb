class EasyshipAPI
  SHIPPING_RATE_URL = 'https://api.easyship.com/rate/v1/rates'
  SHIPMENT_URL = 'https://api.easyship.com/shipment/v1/shipments'
  LABEL_URL = 'https://api.easyship.com/label/v1/labels'
  STATUS_URL = 'https://api.easyship.com/track/v1/status'
  
  def initialize(order = nil, params = {})
    @order = order
    @user_input = params
  end

  def get_shipping_rates
    begin
      shipping_info = shipping_details.merge(combined_items_package_details)
      response = RestClient.post(SHIPPING_RATE_URL, shipping_info, headers)
    rescue RestClient::ExceptionWithResponse => e
      return error_message e
    end

    shipping_rates = JSON(response.body)['rates']
    {
      shippers: shipping_rates.map do |rate|
        {
          courier_id: rate['courier_id'],
          courier_name: rate['courier_name'],
          min_delivery_time: rate['min_delivery_time'],
          max_delivery_time: rate['max_delivery_time'],
          value_for_money_rank: rate['value_for_money_rank'],
          delivery_time_rank: rate['delivery_time_rank'],
          shipment_charge: rate['shipment_charge'],
          shipment_charge_total: rate['shipment_charge_total'],
          insurance_fee: rate['insurance_fee'],
          import_tax_charge: rate['import_tax_charge'],
          import_duty_charge: rate['import_duty_charge'],
          ddp_handling_fee: rate['ddp_handling_fee'],
          total_charge: rate['total_charge'],
          currency: rate['currency']
        }
      end.sort_by { |rate| rate[:total_charge] }
    }
  end

  def buy_label
    shipments = @order.shipments
    return { errors: 'Shipment not created yet.' } if shipments.blank?

    request_param = {
      shipments: shipments.map do |shipment|
        {
        easyship_shipment_id: shipment.easyship_shipment_id,
        courier_id: shipment.courier_id
        }
      end
    }

    begin
      response = RestClient.post(LABEL_URL, request_param, headers)
    rescue RestClient::ExceptionWithResponse => e
      return error_message e
    end

    JSON(response.body)
  end

  def create_shipment
    easyship_params = shipping_details.merge("selected_courier_id" => @user_input[:selected_courier_id])
    begin
      response = RestClient.post(SHIPMENT_URL, easyship_params.merge(combined_items_package_details), headers)
    rescue RestClient::ExceptionWithResponse => e
      return error_message e
    end

    JSON(response.body)
  end

  def delete_shipment easyship_shipment_id
    begin
      response = RestClient.delete("#{SHIPMENT_URL}/#{easyship_shipment_id}", headers)
    rescue RestClient::ExceptionWithResponse => e
      return error_message e      
    end
    
    JSON(response.body)
  end

  def update_shipment easyship_shipment_id
    easyship_params = shipping_details.merge("selected_courier_id" => @user_input[:selected_courier_id])
    begin
      response = RestClient.patch("#{SHIPMENT_URL}/#{easyship_shipment_id}", easyship_params.merge(combined_items_package_details), headers)
    rescue RestClient::ExceptionWithResponse => e
      return error_message e
    end

    JSON(response.body)
  end

  def status easyship_shipment_id
    begin
      response = RestClient.get("#{STATUS_URL}?easyship_shipment_id=#{easyship_shipment_id}", headers)
    rescue RestClient::ExceptionWithResponse => e
      return error_message e
    end
    
    JSON(response.body)
  end


  private

  def headers
    {
      content_type: 'application/json',
      authorization: 'Bearer '.concat(ENV['EASYSHIP_API_KEY'])
    }
  end

  def shipping_details
    # Reverse the option for 'Sender' and 'Receiver' because Easyship return total_charge with respect to 'Sender'
    # whereas here we are showing total_charge with respect to 'Receiver'
    taxes_duties_payer = @user_input[:taxes_duties_paid_by].eql?('Receiver') ? 'Receiver' : 'Sender'

    shipping_address = @order.shipping_address
    {
      "origin_country_alpha2" => "HK",
      "origin_postal_code" => "WC2N",
      "destination_country_alpha2" => shipping_address.country.iso,
      "destination_city" => shipping_address.city,
      "destination_postal_code" => shipping_address.zipcode,
      "destination_name" => shipping_address.first_name + ' ' + shipping_address.last_name,
      "destination_address_line_1" => shipping_address.address1,
      "destination_address_line_2" => shipping_address.address2,
      "destination_phone_number" => shipping_address.phone,
      "destination_email_address" => @order.email,
      "destination_state" => shipping_address.country.states_required? && shipping_address.state.present? ? shipping_address.state.name : '',
      "taxes_duties_paid_by" => taxes_duties_payer,
      "is_insured" => @user_input[:is_insured].present?
    }
  end

  def combined_items_package_details
    items = @order.variants
    line_items = @order.line_items
    declared_customs_value = 0.0
    line_items.each do |line_item|
      declared_customs_value += line_item.cost_amount
    end
    {
      "items"=> [
        {
          "actual_weight" => items.sum(:weight).to_f,
          "height" => (items.sum(:height)/10.0).to_f,
          "width" => (items.maximum(:width)/10.0).to_f,
          "length" => (items.maximum(:depth)/10.0).to_f,
          "category" => shipping_category(items.first),
          "declared_currency" => items.first.cost_currency,
          "declared_customs_value" => declared_customs_value.to_f,
          "description" => items.collect(&:description).join("\n").truncate(200)
        }
      ]
    }
  end

  def shipping_category item
    item_category = item.shipping_category.name
    return item_category if allowed_easyship_item_categories.include? item_category
    'home_decor'
  end

  def allowed_easyship_item_categories
    [
      'mobiles', 'tablets', 'computers_laptops', 'cameras', 'accessory_no_battery',
      'accessory_battery', 'health_beauty', 'fashion', 'watches', 'home_appliances',
      'home_decor', 'toys', 'sport', 'luggage', 'audio_video', 'documents', 'jewelry',
      'dry_food_supplements', 'books_collectionables', 'pet_accessory', 'gaming', 'sauce'
    ]
  end

  def error_message e
    JSON(e.response.body) rescue { 'errors' => [e.response.body] }
  end
end
