Spree::Stock::Estimator.class_eval do
  def shipping_rates(package, frontend_only = true)
    raise ShipmentRequired if package.shipment.nil?
    raise OrderRequired if package.shipment.order.nil?

    # Easyship and Solidus database both are fetched
    # rates = easyship_shipping_rates(package) + calculate_shipping_rates(package)
    # rates.uniq! { |rate| rate.shipping_method_id }

    rates = easyship_shipping_rates(package)
    rates.select! { |rate| rate.shipping_method.available_to_users? } if frontend_only
    rates
  end

  def easyship_shipping_rates(package)
    shipment = package.shipment

    user_input = { taxes_duties_paid_by: shipment.taxes_duties_paid_by, is_insured: shipment.is_insured }
    easyship_rates = EasyshipAPI.new(package.order, user_input).get_shipping_rates

    shipping_rates = []

    return [] unless (easyship_rates[:shippers] && easyship_rates[:shippers].any?)

    easyship_rates[:shippers].each do |rate|
      spree_rate = Spree::ShippingRate.new(
        cost: rate[:total_charge],
        min_delivery_time: rate[:min_delivery_time],
        max_delivery_time: rate[:max_delivery_time],
        value_for_money_rank: rate[:value_for_money_rank],
        shipping_method: find_or_create_shipping_method(rate, package.order.ship_address)
      )

      shipping_rates << spree_rate if spree_rate.shipping_method.frontend?
    end

    # selecting min value_for_money_rank as default
    shipping_rates.min_by(&:value_for_money_rank).selected = true

    # sorting rates based on value_for_money_rank
    shipping_rates.sort_by(&:value_for_money_rank)
  end

  private

  # Cartons require shipping methods to be present, This will lookup a
  # Shipping method based on the admin(internal)_name. This is not user facing
  # and should not be changed in the admin.
  def find_or_create_shipping_method(rate, address)
    method_name = rate[:courier_name]
    Spree::ShippingMethod.find_or_create_by(admin_name: method_name) do |r|
      r.name = method_name
      r.courier_id = rate[:courier_id]
      r.display_on = 'back_end'
      r.code = method_name
      r.available_to_users = true
      r.calculator = EasyshipStore::Calculator::Shipping::EasyshipShippingCalculator.create
      r.shipping_categories = [Spree::ShippingCategory.first]
      r.zones = Spree::Zone.for_address(address)
    end
  end
end
