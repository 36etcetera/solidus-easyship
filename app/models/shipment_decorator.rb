Spree::Shipment.class_eval do

  def display_shipment_charge
    Spree::Money.new(shipment_charge)
  end
  
  def display_shipment_charge_total
    Spree::Money.new(shipment_charge_total)
  end
  
  def display_insurance_fee
    Spree::Money.new(insurance_fee)
  end
  
  def display_import_tax_charge
    Spree::Money.new(import_tax_charge)
  end
  
  def display_import_duty_charge
    Spree::Money.new(import_duty_charge)
  end
  
  def display_ddp_handling_fee
    Spree::Money.new(ddp_handling_fee)
  end
  
  def display_total_charge
    Spree::Money.new(total_charge)
  end
  
end
