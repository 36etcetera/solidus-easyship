Spree::Order.class_eval do
  
  def ensure_updated_shipments
    if !completed? && shipments.all?(&:pending?)
      shipments.each do |shipment|
        if shipment.easyship_shipment_id
          EasyshipAPI.new(self, nil).delete_shipment(shipment.easyship_shipment_id)
        end
      end
      shipments.destroy_all
      update_column(:shipment_total, 0)
      restart_checkout_flow
    end
    
  end
  
end