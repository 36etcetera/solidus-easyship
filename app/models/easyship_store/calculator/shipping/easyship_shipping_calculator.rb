module EasyshipStore
  module Calculator
    module Shipping
      class EasyshipShippingCalculator < Spree::ShippingCalculator

        def self.description
          'Easyship Shipping Calculator'
        end

        def compute_package(package)
          0 # TODO get rate from Easyship depends on the package information
        end

        def available?(order)
          true
        end
      end
    end
  end
end
