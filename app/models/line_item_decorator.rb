Spree::LineItem.class_eval do

  # @return [BigDecimal] the amount of this line item, which is the line
  #   item's price multiplied by its quantity.
  def cost_amount
    cost_price * quantity
  end

end