Spree::CheckoutHelper.class_eval do
  def courier_name name
    if name.starts_with?("HK Post")
      return name.prepend("* ")
    else
      return name
    end
  end
end